/**
 * This Class is constains information about a Particicpant's
 * height, weight, name,as well as a method to calculate the BMI.
 * @author Riles Leroy
 */

public class Participant {
  private String name;
  private int weight;
  private double height;
  
  /**
   * @return the value of the participants name
   */
  public String getName(){return name;}
  
  /**
   * @return the value of the participants weight
   */
  public int getWeight(){return weight;}  
  
  /**
   * @return the value of the participants height
   */
  public double getHeight(){return height;}
  
  /**
   * @return the calculated BMI of the participant
   */
  public double getBMI(){return (weight * 703)/(height * height);}
  
  /**
   * This method sets the name of the Participant
   * @param valName the Participants new name
   */
  public void setName (String valName)
  {
    name = valName;
  }
  
  /**
   * The method returns a formated string of the participant.
   * @return a formated string of the the given participant.
   */
  public String toString()
  {
    String tempString = new String(
                                   "Participant:\n" +
                                   "Name: " + name + "\n" +
                                   "Weight: " + weight + "\n" + 
                                   "Height: " + height + "\n"
                                  );
    return tempString;
  }
 
  /**
   * This method changes the weight of the participant
   * @param valWeight the new weight of the participant
   * @return a boolean informing the user if the value was set
   */
  public boolean setWeight(int valWeight)
  {
    if (valWeight > 0)
    {
      weight = valWeight;
      return true;
    }
    else
    {
      return false;
    }
  }
  
  /**
   * This method changes the height of the participant
   * @param valHeight the new height of the participant
   * @return a boolean informing the user if the value was set
   */
  public boolean setHeight(double valHeight)
  {
    if (valHeight > 0)
    {
      height = valHeight;
      return true;
    }
    else
    {
      return false;
    }
  }
  
  /**
   * Default Constructor
   * @return a default constructed participant
   */
  public Participant()
  {
    name = "Joe Schmoe";
    weight = 200;
    height = 6.0;
  }
  
  /**
   * Name constructor
   * @param initName the string that will be used for the participants name
   * @return a new participant with the name provide
   */
  public Participant(String initName)
  {
    name = initName;
    weight = 200;
    height = 6.0;
  }
  
  /**
   * Standard Constructor
   * @param initName the string that will be used for the participants name
   * @param initWeight the value that the weight of the participant will be initalized to.
   * @param initHeight initWeight the value that the height of the participant will be initalized to.
   * @return a new particpant initialized withe the values provided
   */
  public Participant(String initName, int initWeight, double initHeight)
  {
    name = initName;
    if (initWeight > 0)
    {
      weight = initWeight;
    }
    else
    {
      System.out.println("Incorrect weitght, height must be postive. Weight will be set to 200");
      weight = 200;
    }
    if (initHeight > 0)
    {
      height = initHeight;
    }
    else
    {
      System.out.println("Incorrect height, height must be postive. Height will be set to 6.0");
      height = 6.0;
    }
  }
}